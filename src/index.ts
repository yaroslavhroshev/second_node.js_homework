const { v4: uuidv4 } = require('uuid');

enum Currency {
  USD = 'USD',
  UAN = 'UAN',
}

class Transaction {
  id: string;
  amount: number;
  currency: Currency;

  constructor(amount: number, currency: Currency) {
    this.id = uuidv4();
    this.amount = amount;
    this.currency = currency;
  }
}

class Card {
  private transactionList: Transaction[] = [];

  addTransaction(transactionList: Transaction): string;
  addTransaction(currency: Currency, amount: number): string;
  addTransaction(firstParam: Transaction | Currency, secondParam?: number): string {
    if (firstParam instanceof Transaction) {
      this.transactionList.push(firstParam);
      return firstParam.id;
    } else if (secondParam) {
      const transaction = new Transaction(secondParam, firstParam);
      this.transactionList.push(transaction);
      return transaction.id;
    } else {
      throw new Error('Something went wrong');
    }
  }

  getTransaction(id: string): Transaction | undefined {
    return this.transactionList.find((transaction) => transaction.id === id);
  }

  getBalance(currency: Currency): string {
    const totalSum = this.transactionList
      .filter((transaction) => transaction.currency === currency)
      .reduce((acc, transaction) => acc + transaction.amount, 0);
    return `${totalSum} ${currency}`;
  }
}

const visa = new Card();
const firstTransaction = visa.addTransaction(Currency.USD, 245);
const secondTransaction = visa.addTransaction(new Transaction(5000, Currency.UAN));
console.log(firstTransaction);
console.log(secondTransaction);

const checkFirstTransaction = visa.getTransaction(firstTransaction);
console.log('Transaction 1 Details:', checkFirstTransaction);

const balanceUSD = visa.getBalance(Currency.USD);
console.log('Balance in USD:', balanceUSD);

const balanceUAH = visa.getBalance(Currency.UAN);
console.log('Balance in UAH:', balanceUAH);
